package ru.atonica.arctic;

/**
 * Created with IntelliJ IDEA.
 * User: sgray
 * Date: 26.03.13
 * Time: 15:28
 * To change this template use File | Settings | File Templates.
 */
public class SceneManager implements WallpaperSettings.ISettingsChanged {

    private final static SceneManager INSTANCE = new SceneManager();

    public static SceneManager getInstance() {
        return INSTANCE;
    }

    public SceneManager() {

    }

    public ManagedScene currentScene;
    private ManagedScene nextScene;

    public void showScene(final ManagedScene pManagedScene) {
        nextScene = pManagedScene;
        ResourceManager.getInstance().engine.setScene(nextScene);
        if(currentScene != null) {
            currentScene.onHideManagedScene();
            currentScene.onUnloadManagedScene();
        }
        nextScene.onLoadManagedScene();
        nextScene.onShowManagedScene();
        currentScene = nextScene;
    }

    public void showDayScene() {
        showScene(new DayScene());
    }

    public void showNightScene() {
        showScene(new NightScene());
    }

    @Override
    public void onSettingsChanged(String key) {
        currentScene.onSettingsChangedManagedScene(key);
    }
}
