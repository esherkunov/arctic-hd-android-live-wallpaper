package ru.atonica.arctic;

import android.app.Activity;
import android.app.WallpaperManager;
import android.content.ComponentName;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

/**
 * Created with IntelliJ IDEA.
 * User: sgray
 * Date: 21.03.13
 * Time: 10:10
 * To change this template use File | Settings | File Templates.
 */
public class RemainingProductsActivity extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.remaining_products_activity);
        Button setWallpeperButton = (Button)findViewById(R.id.setWallpaperButton);
        setWallpeperButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                if (Build.VERSION.SDK_INT > 15) {
                    i.setAction(WallpaperManager.ACTION_CHANGE_LIVE_WALLPAPER);
                    i.putExtra(WallpaperManager.EXTRA_LIVE_WALLPAPER_COMPONENT, new ComponentName(
                            ArcticWallpaperService.class.getPackage().getName(),
                            ArcticWallpaperService.class.getCanonicalName()
                    ));
                } else {
                    i.setAction(WallpaperManager.ACTION_LIVE_WALLPAPER_CHOOSER);
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.choose_lwp_text), Toast.LENGTH_LONG).show();
                }
                startActivity(i);
                finish();
            }
        });
        String intentAction = getIntent().getAction();
        if(intentAction != null && intentAction.equals("ru.atonica.arctic.prefshow")) {
            setWallpeperButton.setVisibility(View.GONE);
        }
        ((ImageView)findViewById(R.id.islandImage)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=ru.atonica.island")));
            }
        });
        ((ImageView)findViewById(R.id.canyonImage)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=ru.atonica.canyon")));
            }
        });
        ((ImageView)findViewById(R.id.lanternsImage)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=ru.che3d.android.lw.lantern")));
            }
        });
    }
}