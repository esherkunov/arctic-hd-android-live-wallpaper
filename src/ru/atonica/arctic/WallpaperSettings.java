package ru.atonica.arctic;

import android.content.Context;
import android.content.SharedPreferences;
import org.andengine.engine.camera.Camera;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created with IntelliJ IDEA.
 * User: sgray
 * Date: 26.03.13
 * Time: 12:07
 * To change this template use File | Settings | File Templates.
 */
public class WallpaperSettings implements SharedPreferences.OnSharedPreferenceChangeListener {

    private final static WallpaperSettings INSTANCE = new WallpaperSettings();

    public static WallpaperSettings getInstance() {
        return INSTANCE;
    }

    public WallpaperSettings() {

    }

    public final static String PREFERENCES_NAME = "arctic_settings";

    private SharedPreferences sharedPreferences;

    public final static String SNOW_ENABLED_KEY = "snow_enabled";
    public final static String SNOW_FREQUENCY_KEY = "snow_frequency";
    public final static String BIRDS_IN_FLOCK_KEY = "birds_in_flock";
    public final static String SUNRISE_TIME_KEY = "sunrise_time";
    public final static String SUNSET_TIME_KEY = "sunset_time";
    public final static String SCENE_TYPE_KEY = "scene_type";

    public final static int AURORA_FRAME_DURATION = 150;
    public final static float FAN_VELOCITY_NORMAL = 170f;
    public final static float FAN_VELOCITY_FAST = 340f;
    public final static long SMOKE_FRAME_DURATION_NORMAL = 120;
    public final static long SMOKE_FRAME_DURATION_FAST = 60;

    private boolean snowEnabled;
    private SnowFrequency snowFrequency;
    private BirdsInFlock birdsInFlock;
    private SceneType sceneType;
    private String sunriseTime;
    private String sunsetTime;

    public Camera camera;

    private ISettingsChanged settingsChangedListener;

    public void init(Context pContext) {
        if(sharedPreferences == null) {

            sharedPreferences = pContext.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
            sharedPreferences.registerOnSharedPreferenceChangeListener(this);

            reloadSettings();
        }
    }

    public void setSettingsChangedListener(ISettingsChanged pSettingsChangedListener) {
        settingsChangedListener = pSettingsChangedListener;
    }

    private void reloadSettings() {
        snowEnabled = sharedPreferences.getBoolean(SNOW_ENABLED_KEY, true);
        snowFrequency = SnowFrequency.valueOf(sharedPreferences.getString(SNOW_FREQUENCY_KEY, SnowFrequency.PERIODICALLY.name()));
        birdsInFlock = BirdsInFlock.valueOf(sharedPreferences.getString(BIRDS_IN_FLOCK_KEY, BirdsInFlock.LITTLE.name()));
        sceneType = SceneType.valueOf(sharedPreferences.getString(SCENE_TYPE_KEY, SceneType.DAY.name()));
        sunriseTime = sharedPreferences.getString(SUNRISE_TIME_KEY, "8:00");
        sunsetTime = sharedPreferences.getString(SUNSET_TIME_KEY, "20:00");
    }

    public boolean isSnowEnabled() {
        return snowEnabled;
    }

    public SceneType getSceneType() {
        return sceneType;
    }

    public SceneType getAutoSceneType() {
        SceneType st;
        Calendar currentCal = Calendar.getInstance();

        Calendar sunriseCal = Calendar.getInstance();
        Calendar sunsetCal = Calendar.getInstance();

        sunriseCal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(sunriseTime.split(":")[0]));
        sunriseCal.set(Calendar.MINUTE, Integer.parseInt(sunriseTime.split(":")[1]));
        sunsetCal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(sunsetTime.split(":")[0]));
        sunsetCal.set(Calendar.MINUTE, Integer.parseInt(sunsetTime.split(":")[1]));
        sunriseCal.roll(Calendar.HOUR_OF_DAY, 0);
        sunsetCal.roll(Calendar.HOUR_OF_DAY, 0);

        if(currentCal.compareTo(sunriseCal) >= 0 && currentCal.compareTo(sunsetCal) < 0) {
            st = SceneType.DAY;
        } else {
            st = SceneType.NIGHT;
        }
        return st;
    }

    public boolean isAutoSceneTypeChanged(SceneType pCurrentSceneType) {
        return (getAutoSceneType() != pCurrentSceneType);
    }

    public int getBirdsInFlockCount() {
        return birdsInFlock.getBirdsCount();
    }

    public int getSnowFrequency() {
        return snowFrequency.getFrequency();
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        reloadSettings();
        if(settingsChangedListener != null) {
            settingsChangedListener.onSettingsChanged(key);
        }
    }

    public static enum BirdsInFlock {
        LITTLE { public int getBirdsCount() { return 7; } },
        MANY { public int getBirdsCount() { return 14; } },
        LOT { public int getBirdsCount() { return 21; } };
        public abstract int getBirdsCount();
    }
    public static enum SnowFrequency {
        RARELY { public int getFrequency() { return 160; } },
        PERIODICALLY { public int getFrequency() { return 80; } },
        OFTEN { public int getFrequency() { return 50; } },
        PERMANENTLY { public int getFrequency() { return 0; } };
        public abstract int getFrequency();
    }

    public static enum SceneType {
        AUTO, DAY, NIGHT
    }

    public static interface ISettingsChanged {
        public void onSettingsChanged(String key);
    }


}
