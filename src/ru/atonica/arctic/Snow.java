package ru.atonica.arctic;

import android.util.Log;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.AlphaModifier;
import org.andengine.entity.modifier.IEntityModifier;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.modifier.IModifier;

/**
 * Created with IntelliJ IDEA.
 * User: sgray
 * Date: 28.03.13
 * Time: 11:56
 * To change this template use File | Settings | File Templates.
 */
public class Snow extends AnimatedSprite implements ITimerCallback {

    private static final String TAG = "Arctic";

    public static interface ISnowEvents {
        public void snowStarted();
        public void snowStopped();
    }

    public void setSnowEventsListener(ISnowEvents pSnowEvents) {
        snowEventsListener = pSnowEvents;
    }

    private TimerHandler startSnowTimer;
    private TimerHandler stopSnowTimer;
    private boolean snowStartTimerRegistered = false;
    private ISnowEvents snowEventsListener;

    public Snow(float pX, float pY, ITiledTextureRegion pTiledTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager) {
        super(pX, pY, pTiledTextureRegion, pVertexBufferObjectManager);
        startSnowTimer = new TimerHandler(WallpaperSettings.SnowFrequency.PERIODICALLY.getFrequency(), false, this) {
            @Override
            public String toString() {
                return "start";
            }
        };
        stopSnowTimer = new TimerHandler(40, false, this) {
            @Override
            public String toString() {
                return "stop";
            }
        };
    }

    public void stopSnow() {
        this.setVisible(false);
        this.setIgnoreUpdate(true);
        this.clearUpdateHandlers();
        this.clearEntityModifiers();
        snowStartTimerRegistered = false;
        if(snowEventsListener != null) {
            snowEventsListener.snowStopped();
        }
    }

    public void startSnow() {
        stopSnow();
        this.setIgnoreUpdate(false);
        this.setVisible(true);
        this.animate(100);
        if(WallpaperSettings.getInstance().getSnowFrequency() == 0) {
            return;
        }
        startSnowTimer.setTimerSeconds(WallpaperSettings.getInstance().getSnowFrequency());
        this.registerUpdateHandler(stopSnowTimer);
        if(snowEventsListener != null) {
            snowEventsListener.snowStarted();
        }
    }

    public void setIsSnowy(boolean snowy) {
        if(snowy) {
            startSnow();
        } else {
            stopSnow();
        }
    }

    @Override
    public void onTimePassed(TimerHandler pTimerHandler) {
        final String s = pTimerHandler.toString();
//        Log.i("Arctic", "timer: "+s+" registered: "+snowStopTimerRegistered+" timeelapsed:" + pTimerHandler.getTimerSecondsElapsed());
        if(s.equals("start")) {
            stopSnowTimer.reset();
            this.animate(100);
            this.setAlpha(0f);
            this.setVisible(true);
            AlphaModifier snowAppearanceModifier = new AlphaModifier(4, 0f, 1f);
            snowAppearanceModifier.setAutoUnregisterWhenFinished(true);
            this.registerEntityModifier(snowAppearanceModifier);
            Log.i(TAG, "snow started");
            if(snowEventsListener != null) {
                snowEventsListener.snowStarted();
            }
        }
        if(s.equals("stop")) {
            if(!snowStartTimerRegistered) {
                this.registerUpdateHandler(startSnowTimer);
                snowStartTimerRegistered = true;
            } else {
                startSnowTimer.reset();
            }

            AlphaModifier snowDisappearanceModifier = new AlphaModifier(4, 1f, 0f, new IEntityModifier.IEntityModifierListener() {
                @Override
                public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {
                }

                @Override
                public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
                    ((AnimatedSprite)pItem).stopAnimation();
                    pItem.setVisible(false);
                    Log.i(TAG, "snow stopped");
                }
            });
            snowDisappearanceModifier.setAutoUnregisterWhenFinished(true);
            this.registerEntityModifier(snowDisappearanceModifier);

            if(snowEventsListener != null) {
                snowEventsListener.snowStopped();
            }
        }
    }
}
