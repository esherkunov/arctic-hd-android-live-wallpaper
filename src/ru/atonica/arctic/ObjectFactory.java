package ru.atonica.arctic;

import android.util.Log;
import org.andengine.entity.Entity;
import org.andengine.entity.IEntity;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.texture.ITexture;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.texture.region.ITiledTextureRegion;

/**
 * Created with IntelliJ IDEA.
 * User: sgray
 * Date: 26.03.13
 * Time: 11:15
 * To change this template use File | Settings | File Templates.
 */
public class ObjectFactory {

    private static WallpaperSettings.SceneType sceneType = WallpaperSettings.SceneType.DAY;

    public static void setSceneType(final WallpaperSettings.SceneType pSceneType) {
        sceneType = pSceneType;
    }
    //Наши текстурки облачков обязательно должны быть прилеплены к верхней части экрана,
    // для понимания можно взглянуть на сами текстурки.
    public static Cloud createLittleCloud(float pVelocity) {
        final Cloud cloud = new Cloud(0, 0, pVelocity, ResourceManager.dayLittleCloudTextureRegion, ResourceManager.getInstance().engine.getVertexBufferObjectManager());
        cloud.setPosition(129+cloud.getWidth()/2, Constants.SCENE_HEIGHT-cloud.getHeight()/2);
        return cloud;
    }

    public static Cloud createMediumCloud(float pVelocity) {
        ITextureRegion textureRegion;
        switch (sceneType) {
            case DAY: textureRegion = ResourceManager.dayMediumCloudTextureRegion;
                break;
            case NIGHT: textureRegion = ResourceManager.nightMediumCloudTextureRegion;
                break;
            default: textureRegion = ResourceManager.dayMediumCloudTextureRegion;
        }
        final Cloud cloud = new Cloud(0, 0, pVelocity, textureRegion, ResourceManager.getInstance().engine.getVertexBufferObjectManager());
        cloud.setPosition(257+cloud.getWidth()/2, Constants.SCENE_HEIGHT-cloud.getHeight()/2);
        return cloud;
    }

    public static Cloud createBigCloud(float pVelocity) {
        ITextureRegion textureRegion;
        switch (sceneType) {
            case DAY: textureRegion = ResourceManager.dayBigCloudTextureRegion;
                break;
            case NIGHT: textureRegion = ResourceManager.nightBigCloudTextureRegion;
                break;
            default: textureRegion = ResourceManager.dayBigCloudTextureRegion;
        }
        final Cloud cloud = new Cloud(0, 0, pVelocity, textureRegion, ResourceManager.getInstance().engine.getVertexBufferObjectManager());
        cloud.setPosition(51+cloud.getWidth()/2, Constants.SCENE_HEIGHT-cloud.getHeight()/2);
        return cloud;
    }

    public static AnimatedSprite createWater() {
        ITiledTextureRegion textureRegion;
        switch (sceneType) {
            case DAY: textureRegion = ResourceManager.dayWaterTextureRegion;
                break;
            case NIGHT: textureRegion = ResourceManager.nightWaterTextureRegion;
                break;
            default: textureRegion = ResourceManager.dayWaterTextureRegion;
        }
        final AnimatedSprite water = new AnimatedSprite(0, 0, textureRegion, ResourceManager.getInstance().engine.getVertexBufferObjectManager());
        water.setScale(2.8f);
        water.animate(130);
        water.setPosition(Constants.SCENE_WIDTH/2, water.getHeight()/2*water.getScaleY());
        return water;
    }

    public static Entity createBackground() {
        if(sceneType == WallpaperSettings.SceneType.DAY) {
            final Sprite background = new Sprite(0, 0, ResourceManager.dayBackgroundTextureRegion, ResourceManager.getInstance().engine.getVertexBufferObjectManager());
            background.setIgnoreUpdate(true);
            background.setChildrenIgnoreUpdate(true);
            background.setPosition(Constants.SCENE_WIDTH/2, Constants.SCENE_HEIGHT-background.getHeight()/2*background.getScaleY());
            return background;
        }
        final Entity bg = new Entity();
        final Sprite sky = new Sprite(0, 0, ResourceManager.nightSkyTextureRegion, ResourceManager.getInstance().engine.getVertexBufferObjectManager());
        sky.setScale(2f);
        sky.setPosition(Constants.SCENE_WIDTH/2, Constants.SCENE_HEIGHT-sky.getHeight()/2*sky.getScaleY());
        final Sprite hills = new Sprite(0, 0, ResourceManager.nightHillsTextureRegion, ResourceManager.getInstance().engine.getVertexBufferObjectManager());
        hills.setPosition(Constants.SCENE_WIDTH/2, Constants.SCENE_HEIGHT-110-hills.getHeight()/2*hills.getScaleY());
        bg.attachChild(sky);
        bg.attachChild(hills);
        return bg;
    }

    public static Sprite createNightSky() {
        final Sprite sky = new Sprite(0, 0, ResourceManager.nightSkyTextureRegion, ResourceManager.getInstance().engine.getVertexBufferObjectManager());
        sky.setPosition(Constants.SCENE_WIDTH/2, Constants.SCENE_HEIGHT-sky.getHeight()/2*sky.getScaleY());
        return sky;
    }

    public static Sprite createNightHills() {
        final Sprite hills = new Sprite(0, 0, ResourceManager.nightHillsTextureRegion, ResourceManager.getInstance().engine.getVertexBufferObjectManager());
        hills.setPosition(Constants.SCENE_WIDTH/2, Constants.SCENE_HEIGHT-110-hills.getHeight()/2*hills.getScaleY());
        return hills;
    }

    public static Fan createFan() {
        ITextureRegion textureRegion;
        switch (sceneType) {
            case DAY: textureRegion = ResourceManager.dayFanTextureRegion;
                break;
            case NIGHT: textureRegion = ResourceManager.nightFanTextureRegion;
                break;
            default: textureRegion = ResourceManager.dayFanTextureRegion;
        }
        final Fan fan = new Fan(1002, Constants.SCENE_HEIGHT-281, textureRegion, ResourceManager.getInstance().engine.getVertexBufferObjectManager());
        return fan;
    }

    public static AnimatedSprite createSmoke() {
        final AnimatedSprite smoke = new AnimatedSprite(1037+36, Constants.SCENE_HEIGHT-335+25, ResourceManager.smokeTextureRegion, ResourceManager.getInstance().engine.getVertexBufferObjectManager());
        smoke.animate(WallpaperSettings.SMOKE_FRAME_DURATION_NORMAL);
        if(sceneType == WallpaperSettings.SceneType.DAY) smoke.setAlpha(0.8f);
        else smoke.setAlpha(0.2f);
        return smoke;
    }

    public static Airship createAirship() {
        ITextureRegion textureRegion;
        switch (sceneType) {
            case DAY: textureRegion = ResourceManager.dayAirshipTextureRegion;
                break;
            case NIGHT: textureRegion = ResourceManager.nightAirshipTextureRegion;
                break;
            default: textureRegion = ResourceManager.dayAirshipTextureRegion;
        }
        return new Airship(textureRegion, ResourceManager.getInstance().engine.getVertexBufferObjectManager());
    }

    public static BirdsFlock createBirdsFlock(int pBirdsCount) {
        ITiledTextureRegion textureRegion;
        float alpha;
        switch (sceneType) {
            case DAY: textureRegion = ResourceManager.dayBirdTextureRegion;
                alpha = 1f;
                break;
            case NIGHT: textureRegion = ResourceManager.nightBirdTextureRegion;
                alpha = 0.7f;
                break;
            default: textureRegion = ResourceManager.dayBirdTextureRegion;
                alpha = 1f;
        }
        BirdsFlock birdsFlock = new BirdsFlock(pBirdsCount, 10, 0, textureRegion);
        birdsFlock.setAlpha(alpha);
        return birdsFlock;
    }

    public static Snow createSnow(Snow.ISnowEvents snowEventsListener) {
        ITiledTextureRegion textureRegion;
        switch (sceneType) {
            case DAY: textureRegion = ResourceManager.daySnowTextureRegion;
                break;
            case NIGHT: textureRegion = ResourceManager.nightSnowTextureRegion;
                break;
            default: textureRegion = ResourceManager.daySnowTextureRegion;
        }
        Snow snow = new Snow(0, 0, textureRegion, ResourceManager.getInstance().engine.getVertexBufferObjectManager());
        snow.setScale(2.9f);
        snow.setPosition(Constants.SCENE_WIDTH/2, Constants.SCENE_HEIGHT/2);
        snow.setSnowEventsListener(snowEventsListener);
        snow.setIsSnowy(WallpaperSettings.getInstance().isSnowEnabled());
        return snow;
    }

    public static Aurora createAurora() {
        final Aurora aurora = new Aurora(500, 650, ResourceManager.nightAuroraTextureRegion, ResourceManager.getInstance().engine.getVertexBufferObjectManager());
        aurora.setScale(5f);
        aurora.animateInCicle(WallpaperSettings.AURORA_FRAME_DURATION);
        return aurora;
    }

    public static Grampus createGrampus() {
        final Grampus grampus = new Grampus(960, 250, ResourceManager.grampusTextureRegion, ResourceManager.getInstance().engine.getVertexBufferObjectManager());
        grampus.setScale(1.5f);
        grampus.startInRandomPosition();
        return grampus;
    }
}
