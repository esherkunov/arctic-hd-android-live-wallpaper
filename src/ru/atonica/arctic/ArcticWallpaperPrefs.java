package ru.atonica.arctic;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.*;
import android.preference.Preference.OnPreferenceClickListener;

public class ArcticWallpaperPrefs extends PreferenceActivity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        getPreferenceManager().setSharedPreferencesName(WallpaperSettings.PREFERENCES_NAME);
		addPreferencesFromResource(R.xml.walpaper_prefs);
        PreferenceManager.setDefaultValues(this, R.xml.walpaper_prefs, false);
		final Preference shareThis = findPreference("share_this");
		shareThis.setOnPreferenceClickListener(new OnPreferenceClickListener() {
			
			@Override
			public boolean onPreferenceClick(Preference preference) {
				Intent intent = new Intent(Intent.ACTION_SEND);
				intent.setType("text/plain");
				intent.putExtra(Intent.EXTRA_SUBJECT, "Arctic HD Live Wallpaper");
				intent.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=ru.atonica.arctic");
				startActivity(Intent.createChooser(intent, "Share via"));
				return true;
			}
		});
        final TimePickerPreference sunriseTimePreference = ((TimePickerPreference)findPreference("sunrise_time"));
        sunriseTimePreference.setHoursLimit(4, 10);
        final TimePickerPreference sunsetTimePreference = ((TimePickerPreference)findPreference("sunset_time"));
        sunsetTimePreference.setHoursLimit(16, 23);
        ListPreference sceneTypePreference = (ListPreference)findPreference("scene_type");
        sceneTypePreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                if(newValue.toString().equals("AUTO")) {
                    sunriseTimePreference.setEnabled(true);
                    sunsetTimePreference.setEnabled(true);
                } else {
                    sunriseTimePreference.setEnabled(false);
                    sunsetTimePreference.setEnabled(false);
                }
                return true;
            }
        });
        if(sceneTypePreference.getValue().equals("AUTO")) {
            sunriseTimePreference.setEnabled(true);
            sunsetTimePreference.setEnabled(true);
        } else {
            sunriseTimePreference.setEnabled(false);
            sunsetTimePreference.setEnabled(false);
        }
	}
}
